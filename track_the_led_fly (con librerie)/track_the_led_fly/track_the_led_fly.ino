/********************************
 * GAIA EBLI - matr. 0000882724 *
 ********************************/


#define EI_ARDUINO_INTERRUPTED_PIN
#include <EnableInterrupt.h>
#include <MiniTimerOne.h>
#include "lib.h"

long levels[8] = {4000000, 3500000, 3000000, 2500000, 2000000, 1500000, 1000000, 500000};

void setup() {
  //inizializzo tutte le variabili che valgono per tutta la durata del gioco
  gameStatus = WELCOME;
  currIntensity = 0;
  fadeAmount = 5;
  for (int i = 0; i < 4; i++) {
    pinMode(leds[i], OUTPUT); //imposto i led come output
  }
  pinMode(REDLED_PIN, OUTPUT);
  for (int i = 0; i < 4; i++) {
    pinMode(buttons[i], INPUT); //imposto i bottoni come input
    enableInterrupt (buttons[i], buttonPressed, RISING); //attacco la funzione dell'interrupt ai bottoni
  }
  randomSeed(analogRead(3)); //scelgo il seme
  MiniTimer1.init(); //inizializzo il timer del gioco
  Serial.begin(9600); //faccio partire la seriale
}

void loop() {
  switch (gameStatus) {
    //WELCOME: si inizializzano le variabili che valgono solo per una partita
    case WELCOME:
      MiniTimer1.stop(); //stoppo il timer
      Serial.println("Welcome to the Track to Led Fly Game. Press Key T1 to Start");
      score = 0;
      gameStatus = READY;
      break;
    //READY: faccio il fading del led, setto il livello del gioco, faccio iniziare una partita
    case READY:
      fade();
      tmin = levels[(analogRead(POT_PIN) / 128)]; //leggo il valore del potenziometro e prendo il tempo minimo corrispondente
      if (digitalRead(B1_PIN) == HIGH) {
        digitalWrite(REDLED_PIN, LOW); //spengo il led rosso
        Serial.println("GO!");
        fly = random(4); //scelgo la posizione della mosca e accendo il suo led
        long tmptime = computeTime(); //scelgo il tempo del led
        initTimer(tmptime, timeUp); //inizializzo il timer
        digitalWrite(leds[fly], HIGH); //accendo il led
        gameStatus = PLAYING;
      }
      break;
  }
}
