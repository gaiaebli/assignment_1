#ifndef __LIB_H__
#define __LIB_H__

#define B1_PIN 4
#define B2_PIN 5
#define B3_PIN 9
#define B4_PIN 10
#define L1_PIN 2
#define L2_PIN 3
#define L3_PIN 11
#define L4_PIN 12
#define REDLED_PIN 6
#define POT_PIN A0

#define GAMEOVER 0
#define WELCOME 1
#define READY 2
#define PLAYING 3

extern unsigned char leds[4];
extern unsigned char buttons[4];
extern unsigned char gameStatus;
extern unsigned char score;
extern int currIntensity;
extern int brightness;
extern int fadeAmount;
extern long tmin;
extern long fly;

//funzione che viene chiamata se non premo in tempo un pulsante
void timeUp();

//funzione che viene chiamata quando perdo una partita
void gameOver();

//funzione che richiama lo stato iniziale del gioco
void newGame();

//funzione chiamata quando premo un pulsante
void buttonPressed();

//funzion che inizializza il timer
void initTimer(unsigned long int period, void (*func)(void));

//funzione che calcola il nuovo tempo
long computeTime();

//funzione che sceglie la nuova posizione della mosca
void nextPos();

//funzione che fa il fading
void fade();

//funzione ricorsiva che restituisce l'indice dato il pin di un buttone
unsigned char getBtnIndex(unsigned char pin, unsigned char index);

#endif
