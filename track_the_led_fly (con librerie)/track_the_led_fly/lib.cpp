#include "lib.h"
#include <arduino.h>
#include <MiniTimerOne.h>

extern uint8_t arduinoInterruptedPin; //necessaria per far funzionare la lib EnableInterrupt
unsigned char leds[4] = {L1_PIN, L2_PIN, L3_PIN, L4_PIN};
unsigned char buttons[4] = {B1_PIN, B2_PIN, B3_PIN, B4_PIN};
unsigned char gameStatus;
unsigned char score;
int currIntensity;
int brightness;
int fadeAmount;
long tmin;
long fly;

void timeUp() {
  digitalWrite(leds[fly], LOW); //spegnimento del led
  gameOver();
  Serial.println(String("E' finito il tempo!! Game Over - Score: ") + score);
}

void gameOver() {
  MiniTimer1.stop(); //stoppo il timer
  initTimer(2000000, newGame); //inizializzo un nuovo timer per il led rosso
  digitalWrite(REDLED_PIN, HIGH); //accendo il timer per il led rosso
  gameStatus = GAMEOVER;
}

void newGame() {
  digitalWrite(REDLED_PIN, LOW); //spegne il led rosso
  gameStatus = WELCOME;
}

void buttonPressed() {
  if (gameStatus == PLAYING) {
    //blocco gli interrupts e mi salvo l'inidice del bottone che è stato premuto per fare il controllo
    noInterrupts();
    //arduinoInterruptedPin mi passa il pin dell'ultimo che ha lanciato un interrupt
    unsigned char pressedBtn = getBtnIndex(arduinoInterruptedPin, 0);
    interrupts();
    delayMicroseconds(30000); //previene il bouncing del pulsante
    //caso in cui premo il pulsante giusto durante il gioco
    if (pressedBtn == fly) {
      MiniTimer1.stop(); //stoppo il timer
      digitalWrite(leds[fly], LOW); //spengo il led
      Serial.println(String("Tracking the fly: pos ") + (pressedBtn + 1));
      score++; //aumento il punteggio
      nextPos(); //scelgo la nuova posizione della mosca
      long tmptime = computeTime(); //scelgo il nuovo tempo
      initTimer(tmptime, timeUp); //inizializzo il nuovo timer
      digitalWrite(leds[fly], HIGH); //accendo il nuovo led
    } else if (pressedBtn != fly) { //caso in cui premo il pulsante sbagliato
      digitalWrite(leds[fly], LOW); //spengo il led
      gameOver();
      Serial.println(String("Hai sbagliato bottone!! Game Over - Score: ") + score);
    }
  }
}

void initTimer(unsigned long int period, void (*func)(void)) {
  MiniTimer1.reset(); //resetto il timer
  MiniTimer1.setPeriod(period); //setto il nuovo tempo
  MiniTimer1.attachInterrupt(func); //attacco la nuova funzione di interrupt
  MiniTimer1.start(); //lo faccio partire
}

long computeTime() {
  //entro nell'if solo dopo il primo giro, ovvero dopo aver accesso il primo led
  if (gameStatus == PLAYING) {
    //riduco il tempo secondo il vincolo tmin = 7/8 *tmin
    double tmp = 0.875 * (double)tmin;
    tmin = (long)tmp;
  }
  return random(tmin, 0.5 * tmin); //restituisco un tempo random tra tmin e tmin*k con k=0.5
}

void nextPos() {
  long newPos = random(2); //scelgo un num random tra 0 e 1 (0 = sinistra e 1 = destra)
  if (newPos == 1) {
    fly++; //vado a destra
    if (fly == 4) { //dal led num 3 va al led 0
      fly = 0;
    }
  } else {
    fly--; //vado a sinistra
    if (fly == -1) { //dal led 0 va al led 3
      fly = 3;
    }
  }
}

void fade() {
  analogWrite(REDLED_PIN, currIntensity); //leggo l'intensità corrente
  currIntensity += fadeAmount; //aggiungo/tolgo una quantità predefinita
  //se arrivo al limite inferiore o superiore cambio il segno di fadeAmount per invertire il verso
  if (currIntensity == 0 || currIntensity == 255) {
    fadeAmount = -fadeAmount ;
  }
  delay(15); //aspetto pochissimo tempo per rendere visibile il fade
}

unsigned char getBtnIndex(unsigned char pin, unsigned char index) {
  if (buttons[index] == pin) { //Se il pin all'idice index è uguale al pin passato restituisco quell'indice
    return index;
  } else {
    return getBtnIndex(pin, ++index); //altrimenti richiamo la funzione per l'indice successivo
  }
}
